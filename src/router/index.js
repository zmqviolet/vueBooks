import Vue from 'vue'
import Router from 'vue-router'
import index from '../pages/index'
import filterCom from '../components/filter'
import cartCom from '../components/cart'
import formCom from '../components/form'
import componentCom from '../components/component'
Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index,
    },
    {
      path: '/filter',
      name: 'filter',
      component: filterCom
    },
    {
      path: '/cart',
      name: 'cart',
      component: cartCom
    },
    {
      path: '/form',
      name: 'form',
      component: formCom
    },
    {
      path: '/component',
      name: 'component',
      component: componentCom
    }
  ]
})
